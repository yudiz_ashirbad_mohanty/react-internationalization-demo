import { FormattedMessage } from "react-intl";

export const About = () => {
  return (
    <div className="About">
      <h1 className="ABOUT">
        <FormattedMessage id="ABOUT" />
      </h1>
      <p className="DESCRIPTION">
        <FormattedMessage id="DESCRIPTION" />
      </p>
   
      <p className="">
        <FormattedMessage id="AUTHOR" />
      </p>
    </div>
  );
};
