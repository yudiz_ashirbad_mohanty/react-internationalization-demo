import { useState } from "react";
import { FormattedMessage } from "react-intl";

const Contact = () => {
  const [count, setCount] = useState(0);
  const onChange = () => {
    setCount((prevState) => prevState + 1);
  };

  return (
    <div className="Contact">
      <h2>
        <FormattedMessage id="ORDER" />
      </h2>
      <img src="Book.jpg" alt="img"></img>
      <p>
        <FormattedMessage id="COUNT" values={{ count: count }} />
      </p>
      <button onClick={onChange}>
        <FormattedMessage id="CLICK" />
      </button>
    </div>
  );
};

export default Contact ;
