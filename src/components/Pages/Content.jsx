import { FormattedMessage } from "react-intl";

export const Content = () => {
  return (
    <div className="home-content">
      <h1>
   
        <FormattedMessage id="TITLE" />
      </h1>
      <img src="Book.jpg" alt="img"></img>
      <p>
        <FormattedMessage id="AMMOUNT" values={{ n: 2.63 }} />
      </p>
      <p>
        <FormattedMessage id="NUMBER" values={{ n: 300}} />
      </p>
      <p>
        <FormattedMessage id="DATA" values={{ d: new Date() }} />
      </p>
    </div>
  );
};


