import React, { useState } from "react";
import { NavLink } from "react-router-dom";
export function Navbar() {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  return (
    <>
      <nav>
        <div className="nav-bar">
          <ul>
            <li>
              <NavLink className= "nav-link"
                exact
                to="/"
                onClick={handleClick}
              >
                Home
              </NavLink>
            </li>
            <li>
              <NavLink  className= "nav-link"
                exact
                to="/about"
                onClick={handleClick}
              >
                About 
              </NavLink>
            </li>
            <li>
              <NavLink  className= "nav-link"
                exact
                to="/contact"
                onClick={handleClick}
              >
                Contact 
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
}
