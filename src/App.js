import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home } from "./components/Pages/Home";
import { Aboutbook } from "./components/Pages/Aboutbook";
import { Contactbook } from "./components/Pages/Contactbook ";
import { Navbar } from "./components/Pages/Navbar";
import './App.css'
function App() {
  return (
    <>
      <Router>
      <Navbar/>
        <div className="pages">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={Aboutbook} />
            <Route exact path="/contact" component={Contactbook } />
          </Switch>
        </div>
      </Router>
    </>
  );
}

export default App;
